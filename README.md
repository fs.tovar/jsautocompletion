# JSAutocompletion

## Documentación

* [Neural Complete (based on a generative LSTM neural network)](https://github.com/kootenpv/neural_complete)
* [NEURAL CODE COMPLETION](https://openreview.net/pdf?id=rJbPBt9lg)
* [Code Completion with Neural Attention and Pointer Networks](https://www.ijcai.org/proceedings/2018/0578.pdf)
* [Recurrent Neural Netwoks and LSTM](https://nbviewer.jupyter.org/github/albahnsen/AppliedDeepLearningClass/blob/master/notebooks/20-RecurrentNeuralNetworks_LSTM.ipynb)
* [aroma autocomplete(2019)](https://arxiv.org/abs/1812.01158)
* [lstm skymind tutorial +10](https://skymind.ai/wiki/lstm)
* [RNNs by Example (Abstract Generation)](https://towardsdatascience.com/recurrent-neural-networks-by-example-in-python-ffd204f99470)
* [LSTM original paper](https://www.researchgate.net/publication/13853244_Long_Short-term_Memory)


## Ideas

# Formulation of Task

* Given a sequence of words (code) extract features (e.g n-grams, AST) to Recommend the following element(s) in the sequence.

# AST approximation (Train Dataset generation)

* Take a document (code) extract functions and methods with their respective AST.
* Then create partitions (with different cut-points) of the extracted functions. Preprocess them equally. Now you have X (AST + other features) and y (AST)  
* Try to teach the net to learn the next leaf in the AST or the next non-terminal node in the AST. 
 

# Maybes

* Extra model for predicting lexeme given context and next most-probable non-terminal. 
* Looking at whole code or Looking at just the code until given line.